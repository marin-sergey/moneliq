# Terminal Chart Display Script (`chart.php`)

This is a PHP script that generates charts based on a list of input values. The script takes a list of numbers as input and outputs chart representing the values in a rescaled format.

## Usage

### Running with Host PHP

```bash
echo "1.1066, 1.1048, ... 1.0963" | ./chart.php [TERMINAL_SIZE]
```

### Running with Docker

```bash
echo "1.1066, 1.1048, ... 1.0963" | docker container run --rm -i -v $(pwd):/app/ php:8.2-cli php /app/chart.php [TERMINAL_SIZE]
```

Replace [TERMINAL_SIZE] with an integer representing the desired terminal size (default is 80).

### Task

```bash
echo "1.1066, 1.1048, 1.1023, 1.1003, 1.0969, 1.0951, 1.0901, 1.0914, 1.0867, 1.0842, 1.0835, 1.0816, 1.08, 1.079, 1.0801, 1.0818, 1.084, 1.0875, 1.0964, 1.0977, 1.1122, 1.1117, 1.1125, 1.1187, 1.1336, 1.1456, 1.139, 1.1336, 1.124, 1.1104, 1.1157, 1.0982, 1.0934, 1.0801, 1.0707, 1.0783, 1.0843, 1.0827, 1.0981,1.0977, 1.1034, 1.0956, 1.0936, 1.0906, 1.0785, 1.0791, 1.0885, 1.0871, 1.0867, 1.0963" | ./chart.php
```

or

```bash
echo "1.1066, 1.1048, 1.1023, 1.1003, 1.0969, 1.0951, 1.0901, 1.0914, 1.0867, 1.0842, 1.0835, 1.0816, 1.08, 1.079, 1.0801, 1.0818, 1.084, 1.0875, 1.0964, 1.0977, 1.1122, 1.1117, 1.1125, 1.1187, 1.1336, 1.1456, 1.139, 1.1336, 1.124, 1.1104, 1.1157, 1.0982, 1.0934, 1.0801, 1.0707, 1.0783, 1.0843, 1.0827, 1.0981,1.0977, 1.1034, 1.0956, 1.0936, 1.0906, 1.0785, 1.0791, 1.0885, 1.0871, 1.0867, 1.0963" | docker container run --rm -i -v $(pwd):/app/ php:8.2-cli php /app/chart.php
```

### Pros and cons

#### Pros
1. The script is very simple and easy to understand.
2. The script is very fast and efficient.

#### Cons
1. The script cannot be tested with PHPUnit. For example, to make sure that rescaling works correctly. 
2. The script cannot report about issues using logs and metrics.
3. It is hard to extend with new types of chart (logarithmic, etc.) or output formats (SVG, etc.).