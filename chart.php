#!/usr/bin/env php
<?php

declare(strict_types=1);

/*
 * Inputs
 * arg1 - terminal size. Default 80.
 * STDIN - float numbers separated by space.
 *
 * echo "1.2 1.4 4 5" | ./chart.php 80
 */
if (!$stdin = fgets(STDIN)) {
    die('This script expects numbers as arguments passed through STDIN');
}

$inputLine = trim($stdin);

$inputArray = explode(' ', $inputLine);
if (count($inputArray) < 3) {
    die('This script expects at least 3 numbers as arguments passed through STDIN');
}

$values = array_map('floatval', $inputArray);

$terminalSize = intval($argv[1] ?? 80);

if ($terminalSize < 10 || $terminalSize > 300) {
    die('Invalid terminal size. Should be between 10 and 300');
}

$min = min($values);
$max = max($values);

foreach ($values as $value) {
    $rescaledValue = intval(($value - $min) / ($max - $min) * $terminalSize);
    $dotString = str_repeat(' ', $rescaledValue) . '•' . str_repeat(' ', $terminalSize - $rescaledValue);
    echo $dotString . PHP_EOL;
}

